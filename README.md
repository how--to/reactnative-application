# Prerequisites
To get started you will need three things to follow this article.

- `Node.js`(>=8.12.0)
- `Expo CLI` (>= 2.2.0) 

To install `expo-cli`, please run the following command.
```
npm install -g expo-cli
```

# Init
```
expo init
```
select your `template` and project `name`.

# Setup
## ESLint
```
npm install --save-dev eslint babel-eslint eslint-config-airbnb eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-import
```
Create a configuration file at the root of the project, called `.eslintrc.js` with at least:
```
module.exports = {
  'extends': 'airbnb',
  'parser': 'babel-eslint',
  'env': {
    'jest': true,
  },
  'rules': {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 'off'
  },
  'globals': {
    "fetch": false
  }
}
```
Add a script to `package.json` to lint just javascript files:
```
"lint": "eslint *.js **/*.js"
```